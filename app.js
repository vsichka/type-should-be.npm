/**
 * Type should be.
 * @param {any} data - Data to check.
 * @param {string} type - Type you want to check.
 * @param {any} defaultData - Default data. Returned if data has wrond type.
 * @param {boolean} [showError] - Indicator that error should be shown in case of wrong type.
 * @param {string} [errorMessage] - Error message.
 * @return {any}
 */
function typeShouldBe(data, type, defaultData, showError, errorMessage) {
  // Throw error if wrong type and error should be shown.
  if (showError && typeof data !== type) {
    throw new TypeError(
      errorMessage
      || `Wrong type. Type "${type}" expected but real type is "${typeof data}".`
    );
  }

  // Return defalut data if wrong type.
  if (typeof data !== type) {
    return defaultData;
  }

  // Return data as is in other case.
  return data;
}

// Export.
module.exports = typeShouldBe;
