// Import.
const CColor = require('ccolor');
const tsb = require('./app.js');

// Test data.
const testData = [
  {
    data: 'Some string.',
    type: 'string',
    defaultData: 'Defalut string.',
    result: 'Some string.'
  },
  {
    data: 12345,
    type: 'string',
    defaultData: 'Defalut string.',
    result: 'Defalut string.'
  },
  {
    data: 12345,
    type: 'number',
    defaultData: 11111,
    result: 12345
  },
  {
    data: { a: 1 },
    type: 'number',
    defaultData: null,
    result: null
  }
];

// Run tests.
for (let i = 0; i < testData.length; i++) {
  // Show.
  console.warn('Test ' + (i + 1));

  // Current test data item.
  const current = testData[i];

  // Check returned result.
  const returnedResult = tsb(current.data, current.type, current.defaultData);
  if (returnedResult !== current.result) {
    throw new Error('Returned result should be (' + current.result + '), not (' + returnedResult + ').');
  }

  // Show.
  console.warn(CColor.green('OK!\n'));
}

// Check errors thowing.
console.warn(CColor.cyan('Check errors next.\n'));
try {
  tsb('Some string.', 'number', 'Default string.', true);
} catch (err) {
  console.warn(err);
  console.warn(CColor.green('OK!\n'));
}
try {
  tsb('Some string.', 'number', 'Default string.', true, 'Error here!');
} catch (err) {
  console.warn(err);
  console.warn(CColor.green('OK!\n'));
}

// Show.
console.warn(CColor.green('\nTests passed!'));
