# Type Should Be

![](https://gitlab.com/vsichka/type-should-be.npm/raw/master/logo/logo_256_128_transparent_bg.png)

## Description

Check some data has type as you want.

Returns data as is if data type is correct or default data in other case.

Input params:
* data;
* type;
* default data;
* indicator to show error (not required);
* error message (not required).

## Installation

```sh
npm install type-should-be --save
```

## Initialization

```js
// Import.
const tsb = require('type-should-be');
```

## Using

```js
// Define data.
const data = 'Some string.'

// Type should be a string and it is a string.
const str = tsb(data, 'string', 'Default string.');
// "Some string."

// Type should be a number but it is not a number.
const num = tsb(data, 'number', 10);
// 10

// Type should be an object but it is not an object.
const obj = tsb(data, 'object', { b: 2 });
// { b: 2 }

// Throw error in case of wrong type.
const anotherNum = tsb(data, 'number', 'Default string.', true);
// TypeError: Wrong type. Type "number" expected but real type is "string".

// Throw error with user-defined message in case of wrong type.
const andThisNum = tsb(data, 'number', 'Default string.', true, 'Error here!');
// TypeError: Error here!
```

## License

The MIT License (MIT)

Copyright © 2017 - 2018 Volodymyr Sichka

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
